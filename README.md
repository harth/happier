## Happier

Prettier, happier, more productive.

#### Goals

- 100% deterministic
- Changes formatting behaviour in different contexts
- Context detection is user facing
- Formatting behaviour is user facing
- Formatters should be composable and intuitive
- Fast
- Respects iterative editing
- Elm / Haskell style
- Simple

#### Rationale

##### 100% Deterministic

No original formatting should be retained, by throwing away original formatting we're forced to ensure the formatter meets our needs completely.  Which leads us into...

##### Changes formatting behaviour in different contexts

Hyperscript is formatted differently to normal function calls, css template literals have their own idiosyncracies.  Method chaining, ternaries, etc etc, it's all highly specific.  

Detecting these areas should be something that is exposed to userland and not hard coded - not because debating about styles is worth while, but because there are numerous contexts that should enable new formatting procedures.

##### Context detection is user facing

It should be trivial to define a "context" that requires new formatting rules.  A context is a bounded area of the AST that should be formatted differently.

##### Formatting behaviour is user facing

Once a context is defined, it should be very simple to control how that context is formatted.  It should also be easy to extend/augment/compose/reuse existing printers from other contexts.

##### Formatters should be composable and intuitive

It should be easy to combine and reuse existing printers and form higher level abstractions for printer components that can be combined and reused - a lot like parser combinators (but in reverse).

##### Fast

It needs to be super fast.  If a design decision compromises speed it should be thrown away!

##### Respects iterative editing

The printer shouldn't mess with you while you're still defining a function or an expression.  Especially when used as an editor integration.  I'm not 💯 on how to solve this yet, but it's one of the most frustrating things I've found when using other printers.

##### Elm / Haskell style

Ultimately all these other features are _for me_ to fulfill my needs of having a JS formatter that works with FP style.  If I can accomplish that goal without accomplish the others, I will.  If there's a really simple solution, I'll take it.

##### Simple

The codebase for the printer should be clearly delineated layers that are easy to follow and iterate on.  Where possible the codebase should be data driven and each slice should be testable independently.

## Prior Art

- gofmt
- Elm Format
- Prettier
- Recast

## Resources

- https://astexplorer.net/
